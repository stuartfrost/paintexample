﻿using System.Drawing;

namespace PaintExample.Drawing
{
    public abstract class Grid : Widget
    {
        protected Grid(Canvas canvas) : base(canvas)
        {
        }

        public int Rows { get; set; }
        public int Columns { get; set; }
        public int LineSpacing { get; set; }
        public int LineHeight { get; set; }
        public bool LabelWrapping { get; set; }
        public int RowHeight { get; set; }
        public int ColumnWidth { get; set; }

        protected override void OnPaint()
        {
            base.OnPaint(); // Fill
            DrawGrid();
        }

        protected void DrawGrid()
        {
            DrawLabels();
            DrawCells();
        }

        protected virtual void DrawCells()
        {
            var columns = Columns;
            var rows = Rows;
            for (var column = 0; column < columns; column++)
            {
                for (var row = 0; row < rows; row++)
                {
                    DrawCell(row, column);
                }
            }
        }

        protected virtual void DrawLabels()
        {
            DrawRowLabels();
            DrawColumnLabels();
        }

        private void DrawColumnLabels()
        {
            var columns = Columns;
            for (var column = 0; column < columns; column++)
            {
                DrawColumnLabel(column);
            }
        }

        private void DrawRowLabels()
        {
            var rows = Rows;
            for (var row = 0; row < rows; row++)
            {
                DrawRowLabel(row);
            }
        }

        protected abstract void DrawCell(int row, int column);
        protected abstract void DrawColumnLabel(int column);
        protected abstract void DrawRowLabel(int row);

        protected virtual void DrawCell(Rectangle area, Color colour)
        {
            Canvas.Fill(area, colour);
        }

        protected virtual void DrawLabel(string text, Rectangle area)
        {
            Canvas.DrawLabel(text, area, LineHeight, LineSpacing, LabelWrapping);
        }
    }
}