﻿using System;
using System.Drawing;

namespace PaintExample.Drawing
{
    public class Canvas
    {
        public Canvas() : this(new Rectangle(0, 0, 100, 100))
        {
        }

        public Canvas(Rectangle area)
        {
            Graphics = Graphics.FromHdc(IntPtr.Zero);
            Area = area;
        }

        public Graphics Graphics { get; private set; }
        public Rectangle Area { get; private set; }

        public void Fill(Rectangle area, Color colour)
        {
            DrawRectangle(area, new Pen(colour));
        }

        private void DrawRectangle(Rectangle area, Pen pen)
        {
            var clippedArea = area;
            clippedArea.Intersect(Area); // NB: In place muatation of clippedArea struct
            Graphics.DrawRectangle(pen, clippedArea);
        }

        public void DrawLabel(string text, Rectangle area, int lineHeight, int lineSpacing, bool wordWrap)
        {
            var font = new Font("Times New Roman", lineHeight);
                // NB: Reuse and dispose fonts, they're resource intensive and use unmanaged resources
            var brush = Brushes.Black;
            Graphics.DrawString(text, font, brush, area.Left, area.Top);
        }
    }
}