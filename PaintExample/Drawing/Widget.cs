using System.Drawing;

namespace PaintExample.Drawing
{
    public abstract class Widget
    {
        protected Widget(Canvas canvas)
        {
            Canvas = canvas;
            BackgroundColor = Color.LightGray;
        }

        protected Rectangle Area { get; set; }
        protected Canvas Canvas { get; private set; }
        public Color BackgroundColor { get; set; }

        public void Paint()
        {
            OnPaint();
        }

        protected virtual void OnPaint()
        {
            Canvas.Fill(Area, BackgroundColor);
        }
    }
}