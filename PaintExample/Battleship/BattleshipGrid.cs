﻿using System.Drawing;
using PaintExample.Drawing;

namespace PaintExample.Battleship
{
    public class BattleshipGrid : Grid
    {
        private readonly Color[] _cellColours =
        {
            Color.Red,
            Color.White
        };

        public BattleshipGrid(Canvas canvas) : base(canvas)
        {
            ColumnWidth = 10;
            RowHeight = 10;
            LineSpacing = 2;
            LineHeight = 5;
            LabelWrapping = false;
        }

        protected override void DrawCell(int row, int column)
        {
            var cellNumber = CellNumber(row, column);
            var colour = CellColour(cellNumber);
            var area = CellArea(row, column);
            DrawCell(area, colour);
        }

        protected Rectangle CellArea(int row, int column)
        {
            return new Rectangle(row, column, 1, 1);
        }

        protected int CellNumber(int row, int column)
        {
            return column*Rows + row;
        }

        protected virtual Color CellColour(int cellNumber)
        {
            return _cellColours[cellNumber%2];
        }

        protected override void DrawColumnLabel(int column)
        {
            var text = ColumnText(column);
            var area = ColumnLabelArea(column);
            DrawLabel(text, area);
        }

        protected Rectangle ColumnLabelArea(int column)
        {
            return new Rectangle(ColumnWidth*column, 0, ColumnWidth, RowHeight);
        }

        protected virtual string ColumnText(int column)
        {
            return string.Format("Column {0}", column);
        }

        protected virtual string RowText(int row)
        {
            return string.Format("Row {0}", row);
        }

        protected override void DrawRowLabel(int row)
        {
            var text = RowText(row);
            var area = RowLabelArea(row);
            DrawLabel(text, area);
        }

        protected virtual Rectangle RowLabelArea(int row)
        {
            return new Rectangle(0, RowHeight*row, ColumnWidth, RowHeight);
        }
    }
}