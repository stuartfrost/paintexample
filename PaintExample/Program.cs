﻿using PaintExample.Battleship;
using PaintExample.Drawing;

namespace PaintExample
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var display = new Canvas();
            var grid = new BattleshipGrid(display);
            grid.Paint();
        }
    }
}